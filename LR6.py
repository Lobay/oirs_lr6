import os

import cv2
from matplotlib import pyplot as plt
import numpy as np
from sklearn.metrics import classification_report
import tensorflow as tf

# Размер картинки для входа в сеть
input_img_shape = (32, 32, 3)


def create_fc_network(input_img_shape):
    # Сеть прямого распространения

    fc_model = tf.keras.Sequential([
        # Входной слой для предобработки изображения:
        # приведение значения яркости к [0, 1]
        tf.keras.layers.experimental.preprocessing.Rescaling(
            1. / 255, input_shape=input_img_shape
        ),
        # Представить изображение в виде вектора
        tf.keras.layers.Flatten(),
        # Первый скрытый полносвязный слой с 1024 нейронами
        tf.keras.layers.Dense(1024, activation='relu'),
        # Дропаут слой с вероятностью 0.2
        tf.keras.layers.Dropout(0.2),
        # Второй скрытый полносвязный слой с 64 нейронами
        tf.keras.layers.Dense(64, activation='relu'),
        # Дропаут слой с вероятностью 0.2
        tf.keras.layers.Dropout(0.2),
        # Последний полносвязный слой
        tf.keras.layers.Dense(3)
    ])
    # Скомпилировать модель с оптимизатором Adam
    # Loss-функция - перекрёстная энтропия для мульклассовой классификации
    # Также будем считать метрику - точность
    fc_model.compile(optimizer='adam',
                     loss=tf.keras.losses.SparseCategoricalCrossentropy(
                         from_logits=True),
                     metrics=['accuracy'])
    # Собрать модель
    fc_model.build()

    return fc_model


def create_convolutional_network(input_img_shape):
    conv_model = tf.keras.Sequential([
        tf.keras.layers.experimental.preprocessing.Rescaling(1. / 255,
                                                             input_shape=input_img_shape),
        # Свёрточный слой с 64 картами признаков и размером ядра 5х5
        tf.keras.layers.Conv2D(64, 5, padding='same', activation='relu'),
        tf.keras.layers.MaxPooling2D(),
        # Свёрточный слой с 32 картами признаков и размером ядра 5х5
        tf.keras.layers.Conv2D(32, 3, padding='same', activation='relu'),
        tf.keras.layers.MaxPooling2D(),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(64, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(3)
    ])

    conv_model.compile(optimizer='adam',
                       loss=tf.keras.losses.SparseCategoricalCrossentropy(
                           from_logits=True),
                       metrics=['accuracy'])

    conv_model.build()
    return conv_model


def get_datasets(data_dir, img_size, batch_size, val_split=0.2):
    # Обучающий датасет для изображений из папки
    train_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=val_split,
        subset="training",
        seed=1,
        image_size=img_size,
        batch_size=batch_size)

    # Соответствующий валидационный датасет
    val_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=val_split,
        subset="validation",
        seed=1,
        image_size=img_size,
        batch_size=batch_size)
    return train_ds, val_ds


def save_history_plots(history, title, filename):
    # График обучения полносвязной сети

    keys = ['accuracy', 'val_accuracy', 'loss', 'val_loss']
    colors = ['red', 'blue', 'orange', 'cyan']
    fc_history_dict = history.history

    fig = plt.figure()
    for key, color in zip(keys, colors):
        plt.plot(fc_history_dict[key], label=key, c=color)

    plt.title(title)
    plt.ylabel('Metric')
    plt.xlabel('Epoch')
    plt.legend()
    plt.grid()
    plt.savefig(filename)
    plt.close(fig)


def run_model_on_ds(model, ds):
    # Списки для предсказанных и истинных классов для двух моделей
    result_preds = []
    result_gts = []

    # Пройтись по валидационному датасету
    for img, cls in ds:
        # Выполнить предсказание для картинки с помощью сети
        preds = model.predict(img)
        # Т.к. датасет генерирует данные по батчам, пройдём по всем элементам в батче
        for p in preds:
            result_preds.append(np.argmax(p))
        for c in cls:
            result_gts.append(c)
    return result_preds, result_gts


def make_test_prediction(test_data_dir, results_fc_dir, results_conv_dir,
                    fc_model, conv_model, class_names):
    # Предсказание на тестовых картинках
    if not os.path.exists(results_fc_dir):
        os.makedirs(results_fc_dir)
    if not os.path.exists(results_conv_dir):
        os.makedirs(results_conv_dir)

    # Получить имена всех папок в тестовом датасете
    test_dirs = os.listdir(test_data_dir)
    i = 0
    # Итерироваться по всем папкам
    for d in test_dirs:
        # Получить имена всех картинок в текущей директории тестового датасета
        imgs_fnames = os.listdir(os.path.join(test_data_dir, d))
        # Итерироваться по всем именам картинок в папке
        for img_fname in imgs_fnames:
            # Узнать полный путь к картинке
            img_fpath = os.path.join(test_data_dir, d, img_fname)
            # Открыть картинку и преобразовать порядок цветов из BGR в RGB
            img = cv2.cvtColor(cv2.imread(img_fpath), cv2.COLOR_BGR2RGB)
            # Изменить размер картинки для входа в сеть
            img_resized = cv2.resize(img, (32, 32))[np.newaxis, ...]

            # Предсказание полносвязной сети
            pred_fc = np.argmax(fc_model.predict(img_resized)[0])
            pred_fc_cls_name = class_names[pred_fc]

            # Подписать картинку и сохранить в папку для результатов полносвязной сети
            fig = plt.figure(figsize=(7, 7))
            plt.imshow(img)
            plt.title(f'Predicted: {pred_fc_cls_name}\nCorrect: {d}',
                      fontsize=20)
            plt.xticks([])
            plt.yticks([])
            plt.tight_layout()

            dst_fpath_fc = os.path.join(results_fc_dir, f'{i}.jpg')
            plt.savefig(dst_fpath_fc)
            plt.close(fig)

            # Предсказание свёрточной сети
            pred_conv = np.argmax(conv_model.predict(img_resized)[0])
            pred_conv_cls_name = class_names[pred_conv]

            # Подписать картинку и сохранить в папку для результатов свёрточной сети
            fig = plt.figure(figsize=(7, 7))
            plt.imshow(img)
            plt.title(f'Predicted: {pred_conv_cls_name}\nCorrect: {d}',
                      fontsize=20)
            plt.xticks([])
            plt.yticks([])
            plt.tight_layout()

            dst_fpath_conv = os.path.join(results_conv_dir, f'{i}.jpg')
            plt.savefig(dst_fpath_conv)
            plt.close(fig)

            i += 1


def main():
    # Размер картинки для входа в сеть
    input_img_shape = (32, 32, 3)

    fc_model = create_fc_network(input_img_shape)
    conv_model = create_convolutional_network(input_img_shape)

    # Вывести информацию о моделях
    print('Fully-connected:')
    print(fc_model.summary())

    print('\nConvolutional:')
    print(conv_model.summary())

    # Папка с картинками для обучения
    data_dir = 'dataset/train'
    # Размер валидационной части
    val_split = 0.2
    # Входной размер картинки (без учёта кол-ва каналов)
    img_size = input_img_shape[:2]
    # Размер батча
    bs = 16
    train_ds, val_ds = get_datasets(data_dir, img_size, bs, val_split)

    # Обучение полносвязной сети и запоминание истории метрик
    epochs_fc = 30
    fc_history = fc_model.fit(x=train_ds, epochs=epochs_fc,
                              validation_data=val_ds, batch_size=bs)

    # Обучение свёрточной сети и запоминание истории метрик
    epochs_conv = 15
    conv_history = conv_model.fit(x=train_ds, epochs=epochs_conv,
                                  validation_data=val_ds, batch_size=bs)

    # Запустим модели и получим предсказанные классы и запомним
    # соответствующие истинные классы
    result_preds_fc, result_gts_fc = run_model_on_ds(fc_model, val_ds)
    result_preds_conv, result_gts_conv = run_model_on_ds(conv_model, val_ds)

    # Оценка полносвязной сети
    print(classification_report(result_gts_fc, result_preds_fc,
                                target_names=train_ds.class_names))
    # Оценка свёрточной сети
    print(classification_report(result_gts_conv, result_preds_conv,
                                target_names=train_ds.class_names))

    # Графики обучения сетей
    save_history_plots(fc_history,
                       'Metrics of fully-connected network training',
                       'training_plot_fc.jpg')
    save_history_plots(conv_history,
                       'Metrics of convolutional network training',
                       'training_plot_conv.jpg')

    # Предсказание на тестовых картинках и подпись имён классов
    test_data_dir = 'dataset/test'
    results_fc_dir = 'results_fc'
    results_conv_dir = 'results_conv'
    make_test_prediction(test_data_dir, results_fc_dir, results_conv_dir,
                         fc_model, conv_model, train_ds.class_names)


if __name__ == '__main__':
    main()