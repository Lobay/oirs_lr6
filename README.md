# Вариант 6: Rainier Wolfcastle, Selma Bouvier, Krusty the Clown

**Нейронная сеть прямого распространения**

Обучение скомпилированной модели и оценка полноты, точности и аккуратности:

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    rescaling (Rescaling)        (None, 32, 32, 3)         0         
    _________________________________________________________________
    flatten (Flatten)            (None, 3072)              0         
    _________________________________________________________________
    dense (Dense)                (None, 1024)              3146752   
    _________________________________________________________________
    dropout (Dropout)            (None, 1024)              0         
    _________________________________________________________________
    dense_1 (Dense)              (None, 64)                65600     
    _________________________________________________________________
    dropout_1 (Dropout)          (None, 64)                0         
    _________________________________________________________________
    dense_2 (Dense)              (None, 3)                 195       
    =================================================================
    Total params: 3,212,547
    Trainable params: 3,212,547
    Non-trainable params: 0


                          precision    recall  f1-score   support

      krusty_the_clown       0.95      0.99      0.97       233
    rainier_wolfcastle       1.00      0.17      0.29        12
         selma_bouvier       0.72      0.69      0.71        26

              accuracy                           0.93       271
             macro avg       0.89      0.62      0.65       271
          weighted avg       0.93      0.93      0.91       271


**График потерь и точности:**

![Image alt](https://gitlab.com/Lobay/oirs_lr6/raw/master/graphs/training_plot_fc.jpg)

**Результат работы:**

![Image alt](https://gitlab.com/Lobay/oirs_lr6/raw/master/result_fc/0.jpg)
![Image alt](https://gitlab.com/Lobay/oirs_lr6/raw/master/result_fc/3.jpg)
![Image alt](https://gitlab.com/Lobay/oirs_lr6/raw/master/result_fc/5.jpg)


**Сверточная нейронная сеть**

Обучение скомпилированной модели и оценка полноты, точности и аккуратности:

    Model: "sequential_1"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    rescaling_1 (Rescaling)      (None, 32, 32, 3)         0         
    _________________________________________________________________
    conv2d (Conv2D)              (None, 32, 32, 64)        4864      
    _________________________________________________________________
    max_pooling2d (MaxPooling2D) (None, 16, 16, 64)        0         
    _________________________________________________________________
    conv2d_1 (Conv2D)            (None, 16, 16, 32)        18464     
    _________________________________________________________________
    max_pooling2d_1 (MaxPooling2 (None, 8, 8, 32)          0         
    _________________________________________________________________
    flatten_1 (Flatten)          (None, 2048)              0         
    _________________________________________________________________
    dense_3 (Dense)              (None, 128)               262272    
    _________________________________________________________________
    dropout_2 (Dropout)          (None, 128)               0         
    _________________________________________________________________
    dense_4 (Dense)              (None, 64)                8256      
    _________________________________________________________________
    dropout_3 (Dropout)          (None, 64)                0            
    _________________________________________________________________
    dense_5 (Dense)              (None, 3)                 195       
    =================================================================
    Total params: 294,051
    Trainable params: 294,051
    Non-trainable params: 0

                        precision    recall  f1-score   support

      krusty_the_clown       0.97      1.00      0.99       233
    rainier_wolfcastle       1.00      0.42      0.59        12
         selma_bouvier       0.89      0.92      0.91        26

              accuracy                           0.97       271
             macro avg       0.95      0.78      0.83       271
          weighted avg       0.97      0.97      0.96       271


**График потерь и точности:**

![Image alt](https://gitlab.com/Lobay/oirs_lr6/raw/master/graphs/training_plot_conv.jpg)

**Результат работы:**

![Image alt](https://gitlab.com/Lobay/oirs_lr6/raw/master/result_conv/0.jpg)
![Image alt](https://gitlab.com/Lobay/oirs_lr6/raw/master/result_conv/3.jpg)
![Image alt](https://gitlab.com/Lobay/oirs_lr6/raw/master/result_conv/5.jpg)
 
**Вывод:** на наших данных точность второй модели оказалась выше.

